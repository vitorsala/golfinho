﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour {

	enum Lane{
		TopLane,
		MidLane,
		BottomLane
	}

	public int PlayerLane;
	private Vector3 topPos;
	private Vector3 midPos;
	private Vector3 botPos;

	private bool emTransicao;
	private int PlayerStartX = -6;

	public float moveTime = 0.1f;           //Time it will take object to move, in seconds.
	private float inverseMoveTime;          //Used to make movement more efficient.
	private Rigidbody2D rb2D;               //The Rigidbody2D component attached to this object.


	private Vector2 touchOrigin = -Vector2.one; //Used to store location of screen touch origin for mobile controls.

	private float jumpDuration = 3; 


	// Use this for initialization
	void Start () {
		PlayerLane = (int)Lane.MidLane;
        SharedResources sr = SharedResources.instance;
        midPos = new Vector3 (PlayerStartX, sr.ySpawnPoints[1], 0);
		botPos = new Vector3 (PlayerStartX, sr.ySpawnPoints[0], 0);
		topPos = new Vector3 (PlayerStartX, sr.ySpawnPoints[2], 0);
		this.transform.position = midPos;
		rb2D = GetComponent <Rigidbody2D> ();

		inverseMoveTime = 1f / moveTime;
	}
	
	// Update is called once per frame
	void Update () {
	}

	public void MovePlayer (int vertical){
		if (vertical == 1 && (PlayerLane != (int)Lane.TopLane) && !emTransicao) { 
			if (PlayerLane == (int)Lane.MidLane) {
				print ("mid pra top");
				emTransicao = true;
				PlayerLane = (int)Lane.TopLane;
				StartCoroutine (SmoothMovement (topPos));
				Invoke("jumpEnd",jumpDuration);


			}
			else if (PlayerLane == (int)Lane.BottomLane) {
				print ("bot pra mid");
				emTransicao = true;
				PlayerLane = (int)Lane.MidLane;
				StartCoroutine (SmoothMovement (midPos));

			}

		} 
		if (vertical == -1 && (PlayerLane != (int)Lane.BottomLane) && !emTransicao) {
			if (PlayerLane == (int)Lane.TopLane) {
				print ("top pra mid");
				emTransicao = true;
				PlayerLane = (int)Lane.MidLane;
				StartCoroutine (SmoothMovement (midPos));
				CancelInvoke();

			}
			else if (PlayerLane == (int)Lane.MidLane) {
				print ("mid pra bot");
				emTransicao = true;
				PlayerLane = (int)Lane.BottomLane;
				StartCoroutine (SmoothMovement (botPos));
			}
		}
	}

	protected IEnumerator SmoothMovement (Vector3 end)	{
		//Calculate the remaining distance to move based on the square magnitude of the difference between current position and end parameter. 
		//Square magnitude is used instead of magnitude because it's computationally cheaper.
		float sqrRemainingDistance = (transform.position - end).sqrMagnitude;

		//While that distance is greater than a very small amount (Epsilon, almost zero):
		while(sqrRemainingDistance > float.Epsilon)
		{
			//Find a new position proportionally closer to the end, based on the moveTime
			Vector3 newPostion = Vector3.MoveTowards(transform.position, end, inverseMoveTime * Time.deltaTime);

			//Call MovePosition on attached Rigidbody2D and move it to the calculated position.
			rb2D.MovePosition (newPostion);

			//Recalculate the remaining distance after moving.
			sqrRemainingDistance = (transform.position - end).sqrMagnitude;

			//Return and loop until sqrRemainingDistance is close enough to zero to end the function
			yield return null;
		}
		emTransicao = false;

	}

	public void jumpEnd() {		// obriga o personagem a descer, dando a sensação de pulo
		print("Jump end");
		if (PlayerLane == (int)Lane.TopLane){
			emTransicao = true;
			PlayerLane = (int)Lane.MidLane;
			StartCoroutine (SmoothMovement (midPos));

		}
	}

	void OnTriggerEnter2D(Collider2D other) {
        if(other.tag == ObstacleFactory.obstacleTag) {
            print("HIT");
            other.gameObject.GetComponent<Obstacle>().ReturnToPool();
            //Destroy(other.gameObject);
            GameManager.instance.DecreaseSpeed();
        }
	}
}
