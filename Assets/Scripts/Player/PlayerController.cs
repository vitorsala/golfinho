﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    private Player player;

    private Vector2 touchOrigin = -Vector2.one; //Used to store location of screen touch origin for mobile controls.

    private void Awake() {
        player = gameObject.GetComponent<Player>();
    }

    void Update () {
        if(GameManager.instance.PauseGame == true)
            return;

        int vertical = 0;       //Used to store the vertical move direction.

#if UNITY_STANDALONE || UNITY_WEBPLAYER || UNITY_EDITOR
        //Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
        vertical = (int)(Input.GetAxisRaw("Vertical"));
#endif

#if UNITY_IOS || UNITY_ANDROID || UNITY_WP8 || UNITY_IPHONE

        //Check if Input has registered more than zero touches
        if(Input.touchCount > 0) {
            //Store the first touch detected.
            Touch myTouch = Input.touches[0];

            //Check if the phase of that touch equals Began
            if(myTouch.phase == TouchPhase.Began) {
                //If so, set touchOrigin to the position of that touch
                touchOrigin = myTouch.position;
            }

            //If the touch phase is not Began, and instead is equal to Ended and the x of touchOrigin is greater or equal to zero:
            else if(myTouch.phase == TouchPhase.Ended && touchOrigin.x >= 0) {
                //Set touchEnd to equal the position of this touch
                Vector2 touchEnd = myTouch.position;

                //Calculate the difference between the beginning and end of the touch on the x axis.
                //float x = touchEnd.x - touchOrigin.x;

                //Calculate the difference between the beginning and end of the touch on the y axis.
                float y = touchEnd.y - touchOrigin.y;

                //Set touchOrigin.x to -1 so that our else if statement will evaluate false and not repeat immediately.
                touchOrigin.x = -1;

                //If y is greater than zero, set horizontal to 1, otherwise set it to -1
                vertical = y > 0 ? 1 : -1;
            }

        }

#endif
        if(vertical != 0) {
            //Pass in horizontal and vertical as parameters to specify the direction to move Player in.
            player.MovePlayer(vertical);
        }
    }
}
