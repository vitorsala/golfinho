﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : MonoBehaviour {

    PoolableObject prefab;

    List<PoolableObject> availableObjects = new List<PoolableObject>();

    public PoolableObject GetObject() {
        PoolableObject obj;
        int lastAvailableIndex = availableObjects.Count - 1;
        if(lastAvailableIndex >= 0) {
            obj = availableObjects[lastAvailableIndex];
            availableObjects.RemoveAt(lastAvailableIndex);
            obj.transform.SetParent(obj.previousParent);
            obj.gameObject.SetActive(true);
        }
        else {
            obj = Instantiate<PoolableObject>(prefab);
            obj.transform.SetParent(transform, false);
            obj.pool = this;
        }
        return obj;
    }

    public void AddObject(PoolableObject obj) {
        obj.gameObject.SetActive(false);
        availableObjects.Add(obj);
    }

    public static ObjectPool GetPool(PoolableObject prefab) {
        GameObject obj;
        ObjectPool pool;
#if UNITY_EDITOR
        obj = GameObject.Find(prefab.name + " Pool");
        if(obj) {
            pool = obj.GetComponent<ObjectPool>();
            if(pool) {
                return pool;
            }
        }
#endif
    obj = new GameObject(prefab.name + " Pool");
        pool = obj.AddComponent<ObjectPool>();
        pool.prefab = prefab;
        return pool;
    }
}
