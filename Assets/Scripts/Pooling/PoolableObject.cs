﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PoolableObject : MonoBehaviour {
    public ObjectPool pool { get; set; }

    private Transform _previousParent;
    public Transform previousParent {
        get {
            return _previousParent;
        }
    }

    [System.NonSerialized]
    ObjectPool poolInstanceForPrefab;

    public void ReturnToPool() {
        if(pool) {
            transform.SetParent(pool.transform);
            pool.AddObject(this);
        }
        else {
            Destroy(gameObject);
        }
    }

    public T GetPooledInstance<T>() where T : PoolableObject {
        if(!poolInstanceForPrefab) {
            poolInstanceForPrefab = ObjectPool.GetPool(this);
        }
        return (T)poolInstanceForPrefab.GetObject();
    }
}
