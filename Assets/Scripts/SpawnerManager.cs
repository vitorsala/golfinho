﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnerManager : MonoBehaviour {
	public float movablePoint = 0.0f;

	ObstacleFactory obsFac;
	Transform obstacleContainer;
    SharedResources sharedScript;

    void Awake() {
		obsFac = FindObjectOfType<ObstacleFactory>(); // Set the Obstacle Factory
		obstacleContainer = GameObject.Find("Obstacles").transform; // Get the container (Game object) that will host all of the current obstacle.
	}

    void Start() {
        sharedScript = SharedResources.instance;
    }

    void SpawnNewObstacle(){
        // Create an new obstacle
		GameObject go = obsFac.CreateNewObstacle();
        // Put the obstacle on container.
		go.transform.SetParent(obstacleContainer);

        // Setup
		Obstacle obs = go.GetComponent<Obstacle>();
		Vector2 pos = Vector2.zero;
		switch(obs.type) {
			case ObstacleType.Single:
			case ObstacleType.SingleMovable:
				pos = new Vector2(sharedScript.xSpawnPoint, sharedScript.ySpawnPoints[Random.Range(0, 2)]);
				obs.movablePoint = movablePoint;
				break;

			case ObstacleType.Double:
				pos = new Vector2(sharedScript.xSpawnPoint, (sharedScript.ySpawnPoints[1] + sharedScript.ySpawnPoints[0]) / 2);
				break;

			case ObstacleType.FloatSingle:
				pos = new Vector2(sharedScript.xSpawnPoint, sharedScript.ySpawnPoints[2]);
				break;

			case ObstacleType.FloatDouble:
				pos = new Vector2(sharedScript.xSpawnPoint, (sharedScript.ySpawnPoints[2] + sharedScript.ySpawnPoints[1]) / 2);
				break;
		}
		go.transform.position = pos;
	}


	float timeToSpawn = 0;
	public float spawnRate = 1;
	void Update() {
		if (GameManager.instance.PauseGame == true)
			return;
		
		timeToSpawn -= Time.deltaTime;
		if(timeToSpawn <= 0) {
			SpawnNewObstacle();
			timeToSpawn = 1 / spawnRate;
		}
	}
		
	#if UNITY_EDITOR
	void OnDrawGizmosSelected(){
		Gizmos.color = Color.red;
		Gizmos.DrawSphere(Vector3.right * movablePoint, 0.2f);
	}
	#endif
}
