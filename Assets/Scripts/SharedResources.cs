﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// Class responsible to store values needed by different classes.
/// </summary>
public class SharedResources : MonoBehaviour {
    public static SharedResources _instance = null;
    public static SharedResources instance {
        get {
            return _instance;
        }
    }


    /// <summary>
    /// y points where all lanes belong.
    /// 
    /// <para>
    /// This array aways containt 3 stored values, where the following index represents:
    /// 0 = Top (surface), 1 = Middle and 2 = Bottom.
    /// </para>
    /// </summary>
    [HideInInspector]
	public float[] ySpawnPoints;
    [HideInInspector]
    public float xSpawnPoint;
    [HideInInspector]
    public int worldSpeed = 1;
    [HideInInspector]
    public int speedIncrease = 1;

    private Background bg;

	void Awake () {
        if(instance == null) {
            _instance = this;
        }
        else {
            // Only the duplicated 
            Debug.LogWarning("Duplicated shared resources found!");
            Destroy(this);
            return;
        }

		Rect viewPort = Camera.main.pixelRect;
		Vector3 p;
		ySpawnPoints = new float[3];
		p = Camera.main.ScreenToWorldPoint(new Vector2(0, viewPort.yMax / 6)); // Lane 1
		ySpawnPoints[0] = p.y;
		p = Camera.main.ScreenToWorldPoint(new Vector2(0, 3 * viewPort.yMax / 6)); // Lane 2
		ySpawnPoints[1] = p.y;
		p = Camera.main.ScreenToWorldPoint(new Vector2(viewPort.xMax, 5 * viewPort.yMax / 6)); // Lane 3
		ySpawnPoints[2] = p.y;
		xSpawnPoint = p.x + 2;
		InvokeRepeating("SpeedUP", 2.0f, 2.0f);

        bg = GameObject.FindObjectOfType<Background>();
	}

	public void SpeedUP(){
		worldSpeed += speedIncrease;
        bg.horizontalSpeed = -worldSpeed;
	}

    public void SpeedDown() {
        worldSpeed -= speedIncrease;
        bg.horizontalSpeed = -worldSpeed;
    }

#if UNITY_EDITOR
    // This function will render, on editor, 3 lines that represent the dolphin/obstacles lanes.
    void OnDrawGizmosSelected(){
		Rect viewPort = Camera.main.pixelRect;
		Vector3 p;
		float[] yPoints = new float[3];
		p = Camera.main.ScreenToWorldPoint(new Vector2(0, viewPort.yMax / 6));
		yPoints[0] = p.y;
		p = Camera.main.ScreenToWorldPoint(new Vector2(0, 3 * viewPort.yMax / 6));
		yPoints[1] = p.y;
		p = Camera.main.ScreenToWorldPoint(new Vector2(viewPort.xMax, 5 * viewPort.yMax / 6));
		yPoints[2] = p.y;
		float xPoint = p.x + 2;

		Gizmos.color = Color.cyan;
		foreach(float po in yPoints) {
			Gizmos.DrawLine(new Vector3(-xPoint, po, 0), new Vector3(xPoint, po, 0));
		}

		Gizmos.color = Color.yellow;
		Gizmos.DrawSphere(new Vector3(xPoint, 0, 0), 0.2f);
	}
	#endif
}
