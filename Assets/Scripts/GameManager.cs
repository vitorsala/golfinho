﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public Text speedText;
	SharedResources sharedScript;
	public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script.
	public GameObject PanelGameOver;
	public bool PauseGame = false;

	enum Lanes{
		TopLane,
		MidLane,
		BottomLane
	}

	// Use this for initialization
	void Awake () {
		//Check if instance already exists
		if (instance == null)

			//if not, set instance to this
			instance = this;

		//If instance already exists and it's not this:
		else if (instance != this )

			//Then destroy this. This enforces our singleton pattern, meaning there can only ever be one instance of a GameManager.
			Destroy(gameObject);	

		//Sets this to not be destroyed when reloading scene
		DontDestroyOnLoad(gameObject);
		PanelGameOver = GameObject.Find ("PanelGameOver");
		PanelGameOver.SetActive (false);
		PauseGame = false;

	}

    void Start() {
        sharedScript = SharedResources.instance;
    }
	
	// Update is called once per frame
	void Update () {
        if(GameManager.instance.PauseGame == true) {
            Time.timeScale = 0;
            return;
        }
        else {
            Time.timeScale = 1;
        }
        speedText.text = "Velocidade: " + sharedScript.worldSpeed;
	}

	public void DecreaseSpeed(){
        sharedScript.SpeedDown();
        if (sharedScript.worldSpeed <=0){
			GameOver ();
		}
		speedText.text = "Velocidade: " + sharedScript.worldSpeed;
	}

	public void GameOver(){
		PauseGame = true;
		PanelGameOver.SetActive (true);
	}

	public void RestartGame(){
		Scene scene = SceneManager.GetActiveScene ();
		SceneManager.LoadScene (scene.name);
		Destroy (this.gameObject);
        sharedScript.worldSpeed = 1;
	}
}
