﻿using UnityEngine;
using System.Collections;

public enum ObstacleType {
	Single, SingleMovable, Double, FloatSingle, FloatDouble
}

