﻿using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This class is responsible to create new obstacles.
/// </summary>
public class ObstacleFactory : MonoBehaviour {
    public static string obstacleTag = "Obstacle";

    // Osbacles prefabs.
    [Tooltip("Obsctacles prefabs.\nEvery obstacle must have an Obstacle component, and be added here in order to spawn.")]
	public Obstacle[] prefabs;

    /// <summary>
    /// Create a new random Obstacle.
    /// </summary>
    /// <returns>GameObject representing the new obstacle.</returns>
	public GameObject CreateNewObstacle(){
        //GameObject go = Instantiate(prefabs[Random.Range(0, prefabs.Length)]).gameObject;
        GameObject go = prefabs[Random.Range(0, prefabs.Length)].GetPooledInstance<Obstacle>().gameObject;
        go.tag = obstacleTag;
        return go;
	}

    /// <summary>
    /// Create a new random Obstacle of given type.
    /// </summary>
    /// <param name="type">Obstacle type to spawn.</param>
    /// <returns>GameObject representing the new obstacle.</returns>
	public GameObject CreateNewObstacle(ObstacleType type){
		Obstacle[] chosen = prefabs.Where(obj => obj.type == type).ToArray();

		Debug.Assert(chosen.Length > 0, "Nenhum objeto, com o tipo informado, foi encontrado!");

		//GameObject go = Instantiate(chosen[Random.Range(0, chosen.Length)]).gameObject;
        GameObject go = chosen[Random.Range(0, chosen.Length)].GetPooledInstance<Obstacle>().gameObject;
        go.tag = obstacleTag;

        return go;
	}

}
