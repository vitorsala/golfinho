﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : PoolableObject {
	
	public ObstacleType type;
	public float ObjectVelocity = 1f;
	public float verticalMovementTime = 1f;

	[HideInInspector] public float movablePoint;

	private float velocity;
	SharedResources sharedScript;

	int changeToLine = -1;
	public bool canRotate;

	void Start () {
		sharedScript = SharedResources.instance;
		velocity = ObjectVelocity * sharedScript.worldSpeed;
	}

	void changeLine(){
		if(gameObject.transform.position.y == sharedScript.ySpawnPoints[0]) {
			changeToLine = 1;
		}
		else {
			changeToLine = 0;
		}
	}

	void Update () {
		if (GameManager.instance.PauseGame == true)
			return;
		
		velocity = ObjectVelocity * sharedScript.worldSpeed;
		float xPosition = transform.localPosition.x - (velocity * Time.deltaTime);
		float yPosition = transform.localPosition.y;
		if(type == ObstacleType.SingleMovable && gameObject.transform.localPosition.x <= movablePoint) {
			changeLine();
			type = ObstacleType.Single;
		}
		if(changeToLine != -1) {
			if(yPosition >= sharedScript.ySpawnPoints[0] && yPosition <= sharedScript.ySpawnPoints[1])
				yPosition += (changeToLine == 0 ? -0.1f : 0.1f) / verticalMovementTime;
			else {
				yPosition = sharedScript.ySpawnPoints[changeToLine];
				changeToLine = -1;
			}
		}
		transform.localPosition = new Vector2(xPosition, yPosition);

		if (canRotate)
			transform.Rotate (0, 0, 5);
	
		transform.localPosition = new Vector2(transform.localPosition.x - (velocity * Time.deltaTime), transform.localPosition.y);
	}

	void OnBecameInvisible(){
        //Destroy(gameObject);
        ReturnToPool();
	}
}
